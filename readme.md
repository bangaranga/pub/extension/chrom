# chrom* 



# chrom
## chromium | chrome

***no specific order***          


### extension - guid

`https://chrome.google.com/webstore/detail/`               


native search              
```
https://chrome.google.com/webstore/detail/%s
```                



> context search
>> https://chrome.google.com/webstore/detail/contextsearch-web-ext/ddippghibegbgpjcaaijbacfhjjeafjh                            
```
https://chrome.google.com/webstore/detail/{searchTerms}
```


| guid | url-name
| ---- | -------- 
| cjpalhdlnbpafiamejdnhcphjbkeiagm | ublock-origin                             
| ddippghibegbgpjcaaijbacfhjjeafjh | contextsearch-web-ext                             
| gleekbfjekiniecknbkamfmkohkpodhe | ignore-x-frame-headers                             
| jinjaccalgkegednnccohejagnlnfdag | violentmonkey                             
| kdplapeciagkkjoignnkfpbfkebcfbpb | uautopagerize                             
| gdnpnkfophbmbpcjdlbiajpkgdndlino | infy-scroll                             
| iokagnmembcjfocpbfmdojdghbfjpmkb | nextpage-auto-load-next-p                             
| igiofjhpmpihnifddepnpngfjhkfenbp | autopagerize                             
| mfpgdeedjdffbfoomliadmehaoailnjn | super-bookmark-desktop                             
| mpiodijhokgodhhofbcjdecpffjipkle | singlefile                             
| mpkodccbngfoacfalldjimigbofkhgjn | aria2-for-chrome                             
| pddljdmihkpdfpkgmbhdomeeifpklgnm | vertical-tabs                             
| ppmnkcnnoclkpbbckblofjbjepocnflo | ultra-button                             
| pnmchffiealhkdloeffcdnbgdnedheme | history-trends-unlimited                             
| mefgmmbdailogpfhfblcnnjfmnpnmdfa | feedbro                             
| lfpjkncokllnfokkgpkobnkbkmelfefj | linkclump                             
| bifmfjgpgndemajpeeoiopbeilbaifdo | external-application-laun                             
| jnlkjeecojckkigmchmfoigphmgkgbip | gooreplacer                             
| abdkkegmcbiomijcbdaodaflgehfffed | simple-mass-downloader                             
| jaoafjdoijdconemdmodhbfpianehlon | skip-redirect                             
| ddippghibegbgpjcaaijbacfhjjeafjh | contextsearch-web-ext                             
| lcaelgondnfehcambmdhhfokjknhfahc | bookmarks-bar-switcher                  
| edibdbjcniadpccecjdfdjjppcpchdlm | i-still-dont-care-about-c                   
| ecanpcehffngcegjmadlcijfolapggal | ipvfoo                        


                        
                        



